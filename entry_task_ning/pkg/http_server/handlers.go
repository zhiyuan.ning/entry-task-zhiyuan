package main

import (
	__ "entry_task_ning/pkg/protoc"
	"github.com/gin-gonic/gin"
	"net/http"
)

func indexHandler(c *gin.Context) {
	c.HTML(http.StatusOK, "index.html", gin.H{})
}

func loginHandler(c *gin.Context) {
	username := c.PostForm("username")
	password := c.PostForm("password")
	//Get a connection from the connection pool
	conn := myChannel.GetConnection()
	defer myChannel.ReturnConnection(conn)

	req := __.LoginReq{Username: username, Password: password}
	// communicate with the tcp server through the connection
	res := callLogin(&req, conn)

	user := &User{Username: res.Username, Nickname: res.Nickname, Picture: res.Picture} // deserialize the data

	c.SetCookie("username", res.Username, 1000, "/", "", false, false)
	c.SetCookie("token", res.Token, 1000, "/", "", false, false)

	c.HTML(http.StatusOK, "profile.html", gin.H{
		"user": user,
	}) // render the html
}

func updateHandler(c *gin.Context) {
	nickname := c.PostForm("nickname")
	username, _ := c.Cookie("username")
	token, _ := c.Cookie("token")

	conn := myChannel.GetConnection()
	defer myChannel.ReturnConnection(conn)

	req := __.UpdateReq{Username: username, Nickname: nickname, Token: token}
	res := callUpdate(&req, conn)

	user := &User{Username: res.Username, Nickname: res.Nickname, Picture: res.Picture}

	c.HTML(http.StatusOK, "profile.html", gin.H{
		"user": user,
	})
}

func uploadHandler(c *gin.Context) {
	username, _ := c.Cookie("username")
	token, _ := c.Cookie("token")
	img, _ := c.FormFile("img")
	imgName := img.Filename

	conn := myChannel.GetConnection()
	defer myChannel.ReturnConnection(conn)

	req := __.UploadReq{Username: username, Picture: imgName, Token: token}
	res := callUpload(&req, conn)

	user := &User{Username: res.Username, Nickname: res.Nickname, Picture: res.Picture}

	c.HTML(http.StatusOK, "profile.html", gin.H{
		"user": user,
	})
}

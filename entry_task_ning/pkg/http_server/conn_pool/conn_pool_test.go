package conn_pool

import (
	"github.com/astaxie/beego/logs"
	"testing"
	"time"
)

var myPool *ConnPool

func TestConn_pool(t *testing.T) {
	myPool = NewConnPool(10)
	logs.Debug("test connection")
	time.Sleep(10)
	ClosePool(myPool)
	logs.Debug("Close connection")
}

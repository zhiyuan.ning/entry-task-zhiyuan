package conn_pool

// Using an array of net.conn to implement a connection pool

import (
	"github.com/astaxie/beego/logs"
	"net"
	"sync"
)

type ConnPool struct {
	mutex    sync.Mutex // ensure one goroutine at a time access the ConnPool
	size     []net.Conn
	capacity []net.Conn
}

func (connPool *ConnPool) Len() int { //Return the size of the pool
	return len(connPool.size)
}

func NewConnPool(initCapacity int) *ConnPool { // init a pool
	addr := "127.0.0.1:8081"
	// init two array to hold connections
	capacity := make([]net.Conn, initCapacity)
	size := make([]net.Conn, initCapacity)
	// create connections
	for i := 0; i < initCapacity; i++ {
		conn, err := net.Dial("tcp", addr)
		logs.Debug("Initiate connection: NO.", i)
		for err != nil {
			logs.Critical("Initiate connection error")
			conn, err = net.Dial("tcp", addr)
		}
		capacity[i] = conn
		size[i] = conn
	}
	return &ConnPool{capacity: capacity, size: size}
}

func (connPool *ConnPool) GetConnection() *net.Conn { //fetch a connection from the pool
	connPool.mutex.Lock()
	defer connPool.mutex.Unlock()
	if len(connPool.size) == 0 {
		logs.Critical("No available connections for now!")
		return nil
	}

	conn := connPool.size[len(connPool.size)-1]
	connPool.size = connPool.size[:len(connPool.size)-1]
	logs.Debug("Successfully got a connection from the connection pool!")
	return &conn
}

func (connPool *ConnPool) ReturnConnection(conn *net.Conn) { // return the connection to the pool
	connPool.mutex.Lock()
	defer connPool.mutex.Unlock()

	connPool.size = append(connPool.size, *conn)
	logs.Debug("Successfully return a connection from the connection pool!")
}

func ClosePool(connPool *ConnPool) { // close the pool
	connPool.mutex.Lock()
	defer connPool.mutex.Unlock()

	for _, conn := range connPool.capacity {
		_ = conn.Close()
	}
	logs.Debug("Close the connection pool.")
}

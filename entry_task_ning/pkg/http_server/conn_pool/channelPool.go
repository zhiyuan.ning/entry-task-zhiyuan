package conn_pool

// Using channel to implement a connection pool
import (
	"fmt"
	"github.com/astaxie/beego/logs"
	"net"
	"sync"
)

type ChannelPool struct {
	mu    sync.RWMutex
	conns chan net.Conn
}

func NewChannelPool(initialCap, maxCap int) (*ChannelPool, error) { // init a connection pool
	addr := "127.0.0.1:8081"
	c := &ChannelPool{
		conns: make(chan net.Conn, maxCap),
	}
	for i := 0; i < initialCap; i++ {
		conn, err := net.Dial("tcp", addr)
		if err != nil {
			c.Close()
			return nil, fmt.Errorf("can not init the connection: %s", err) // return if dial failed
		}
		logs.Debug("Initiate connection: NO.", i)
		c.conns <- conn
	}

	return c, nil
}

func (c *ChannelPool) GetConnection() net.Conn { // get a connection from the connection pool
	c.mu.RLock()
	defer c.mu.RUnlock()
	conn := <-c.conns

	if conn == nil {
		logs.Critical("The connection that gonna get is invalid.")
		return nil
	}
	return conn
}

func (c *ChannelPool) ReturnConnection(conn net.Conn) { // return the connection that have been used
	c.mu.RLock()
	defer c.mu.RUnlock()
	if conn == nil {
		logs.Critical("The connection that gonna returned is invalid")
		return
	}
	c.conns <- conn

}

func (c *ChannelPool) Close() { // close the connection pool
	c.mu.Lock()
	conns := c.conns
	c.conns = nil
	c.mu.Unlock()
	if conns == nil {
		return
	}

	close(conns)
	for conn := range conns {
		err := conn.Close()
		if err != nil {
			return
		}
	}
}

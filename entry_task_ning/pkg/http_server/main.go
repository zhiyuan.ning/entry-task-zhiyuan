package main

import (
	"encoding/json"
	"entry_task_ning/config"
	"entry_task_ning/pkg/http_server/conn_pool"
	"fmt"
	"github.com/astaxie/beego/logs"
	"github.com/gin-gonic/gin"
	"github.com/unrolled/secure"
	"net/http"
	"os"
)

//var myPool *conn_pool.ConnPool
var myChannel *conn_pool.ChannelPool
var err error
var httpConfig config.HttpConfig

func init() {

	configFile, err := os.Open("/Users/zhiyuan.ning/go/src/entry_task_ning/config/config.json")
	defer configFile.Close()
	if err != nil {
		fmt.Println(err.Error())
	}

	jsonParser := json.NewDecoder(configFile)
	jsonParser.Decode(&httpConfig)

}

func TlsHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		secureMiddleware := secure.New(secure.Options{
			SSLRedirect: true,
			SSLHost:     "localhost:8080",
		})
		err := secureMiddleware.Process(c.Writer, c.Request)

		// If there was an error, do not continue.
		if err != nil {
			return
		}

		c.Next()
	}
}

func main() {

	//myPool = conn_pool.NewConnPool(config.TCPPoolInitCapacity)
	myChannel, err = conn_pool.NewChannelPool(1000, 1000) // init the pool
	if err != nil {
		return
	}
	defer myChannel.Close()

	router := gin.Default() // init the router
	//router.Use(TlsHandler()) // using TLs

	router.LoadHTMLGlob(httpConfig.HTMLFilePath)
	router.StaticFS("/static", http.Dir("./static"))

	router.GET("/index", indexHandler) // handlers
	router.POST("/update", updateHandler)
	router.POST("/login", loginHandler)
	router.POST("/upload", uploadHandler)

	err := router.Run(httpConfig.HTTPServerAddress)

	// using RunTLS to listening HTTPS request on :8080
	//err := router.RunTLS(
	//	":8080",
	//	"/Users/zhiyuan.ning/go/src/entry_task_ning/pkg/http_server/server.crt",
	//	"/Users/zhiyuan.ning/go/src/entry_task_ning/pkg/http_server/server.key")
	if err != nil {
		logs.Critical("Can not launch the http Server.")
		os.Exit(-1)
	}
}

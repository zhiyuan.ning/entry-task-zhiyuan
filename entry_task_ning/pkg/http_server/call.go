package main

import (
	"entry_task_ning/pkg/protoc"
	"github.com/astaxie/beego/logs"
	"google.golang.org/protobuf/proto"
	"net"
	"time"
)

func callUpdate(req *__.UpdateReq, conn net.Conn) *__.InfoRes {
	conn.SetDeadline(time.Now().Add(60 * time.Second))

	reqMarshal, _ := proto.Marshal(req)

	message := __.Message{Type: "UpdateReq", Req: reqMarshal}
	messageMarshal, _ := proto.Marshal(&message)

	_, err2 := conn.Write(messageMarshal)
	if err2 != nil {
		logs.Critical("Can not write to the data through the connection!")
		return nil
	}
	messageMarshal = make([]byte, 4096)

	messageLength, err := conn.Read(messageMarshal)
	if err != nil {
		logs.Critical("Can not read data from the connection!")
		return nil
	}

	proto.Unmarshal(messageMarshal[:messageLength], &message)
	res := &__.InfoRes{}
	proto.Unmarshal(message.Res, res)

	res.Picture = "../../static/" + res.Picture

	return res
}

func callUpload(req *__.UploadReq, conn net.Conn) *__.InfoRes {
	conn.SetDeadline(time.Now().Add(1 * time.Second))
	reqMarshal, _ := proto.Marshal(req)

	message := __.Message{Type: "UploadReq", Req: reqMarshal}
	messageMarshal, _ := proto.Marshal(&message)

	_, err2 := conn.Write(messageMarshal)
	if err2 != nil {
		logs.Critical("Can not write to the data through the connection!")
		return nil
	}
	messageMarshal = make([]byte, 4096)

	messageLength, err := conn.Read(messageMarshal)
	if err != nil {
		logs.Critical("Can not read data from the connection!")
		return nil
	}
	proto.Unmarshal(messageMarshal[:messageLength], &message)
	res := &__.InfoRes{}
	proto.Unmarshal(message.Res, res)

	res.Picture = "../../static/" + res.Picture
	return res
}

func callLogin(req *__.LoginReq, conn net.Conn) *__.InfoRes {
	//conn.SetDeadline(time.Now().Add(1 * time.Second))
	reqMarshal, _ := proto.Marshal(req) // serialize the buffer data

	message := __.Message{Type: "LoginReq", Req: reqMarshal}
	messageMarshal, _ := proto.Marshal(&message) // put the req into message

	_, err2 := conn.Write(messageMarshal)
	if err2 != nil {
		logs.Critical("Can not write to the data through the connection!")
		return nil
	}

	messageMarshal = make([]byte, 4096)
	messageLength, err := conn.Read(messageMarshal)
	logs.Debug("The login message length is ", messageLength)

	if err != nil {
		logs.Critical("Can not read data from the connection!")
		return nil
	}

	logs.Debug("Read the serialize data to the connection")

	proto.Unmarshal(messageMarshal[:messageLength], &message)
	res := &__.InfoRes{}
	proto.Unmarshal(message.Res, res)

	res.Picture = "../../static/" + res.Picture
	return res
}

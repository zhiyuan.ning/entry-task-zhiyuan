package main

import "github.com/astaxie/beego/logs"

func getUserInfo(username string) (name, psw, nick, pic string) {
	usernameCache, passwordCache, nicknameCache, pictureCache := getCacheUserInfo(username)
	if usernameCache != "" {
		logs.Debug(name, "get info from the cache")
		return usernameCache, passwordCache, nicknameCache, pictureCache
	}

	usernameDb, passwordDb, nicknameDb, pictureDb := getDbUserInfo(username)
	setCacheUserInfo(usernameDb, passwordDb, nicknameDb, pictureDb)
	logs.Debug(name, "set the cache and hit db")

	return usernameDb, passwordDb, nicknameDb, pictureDb
}

func updateNickname(username, nickname string) {
	usernameDb, passwordDb, _, pictureDb := getUserInfo(username)
	setCacheUserInfo(usernameDb, passwordDb, nickname, pictureDb)
	updateDbNickname(username, nickname)
}

func uploadPicture(username, picture string) {
	usernameDb, passwordDb, nicknameDb, _ := getUserInfo(username)
	setCacheUserInfo(usernameDb, passwordDb, nicknameDb, picture)
	uploadDbPicture(username, picture)
}

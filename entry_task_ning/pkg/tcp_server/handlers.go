package main

import (
	__ "entry_task_ning/pkg/protoc"
	"fmt"
	"github.com/astaxie/beego/logs"
	"google.golang.org/protobuf/proto"
	"net"
)

func loginHandler(message *__.LoginReq, conn net.Conn) {
	var usernameDb, passwordDb, nicknameDb, pictureDb string
	//user := model.User{}
	logs.Debug("Start user auth")
	fmt.Println(message.Username)

	usernameDb, passwordDb, nicknameDb, pictureDb = getUserInfo(message.Username)

	if passwordDb != message.Password {
		logs.Critical(conn, "wrong password")
		return
	}
	logs.Debug("User Auth Success")

	token, _ := s.Encode("username", usernameDb)
	res := __.InfoRes{Username: usernameDb, Nickname: nicknameDb, Picture: pictureDb, Token: token}
	resMarshal, _ := proto.Marshal(&res)
	reply := __.Message{Type: "InfoRes", Res: resMarshal}
	replyMarshal, errReply := proto.Marshal(&reply)
	if errReply != nil {
		fmt.Println(errReply)
		logs.Critical(conn, "marshal error")
		return
	}
	conn.Write(replyMarshal)
}

func updateHandler(message *__.UpdateReq, conn net.Conn) {
	var token string
	s.Decode("username", message.Token, &token)
	if token != message.Username {
		logs.Critical(conn, "verity token error")
		return
	}

	updateNickname(message.Username, message.Nickname)
	logs.Debug(message.Username, "change the nick name")

	var usernameDb, nicknameDb, pictureDb string
	usernameDb, _, nicknameDb, pictureDb = getUserInfo(message.Username)

	res := __.InfoRes{Username: usernameDb, Nickname: nicknameDb, Picture: pictureDb}
	resMarshal, _ := proto.Marshal(&res)
	reply := __.Message{Type: "success", Res: resMarshal}
	replyMarshal, errReply := proto.Marshal(&reply)
	if errReply != nil {
		fmt.Println(err)
		logs.Critical(conn, "marshal error")
		return
	}

	conn.Write(replyMarshal)
}

func uploadHandler(message *__.UploadReq, conn net.Conn) {
	var token string
	s.Decode("username", message.Token, &token)
	if token != message.Username {
		logs.Critical(conn, "verity token error")
		return
	}

	uploadPicture(message.Picture, message.Username)
	logs.Debug(message.Username, "upload the picture")

	var usernameDb, nicknameDb, pictureDb string
	usernameDb, _, nicknameDb, pictureDb = getUserInfo(message.Username)

	res := __.InfoRes{Username: usernameDb, Nickname: nicknameDb, Picture: pictureDb}
	resMarshal, _ := proto.Marshal(&res)
	reply := __.Message{Type: "success", Res: resMarshal}
	replyMarshal, errReply := proto.Marshal(&reply)
	if errReply != nil {
		fmt.Println(err)
		logs.Critical(conn, "marshal error")
		return
	}
	conn.Write(replyMarshal)
}

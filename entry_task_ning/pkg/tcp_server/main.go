package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"entry_task_ning/config"
	__ "entry_task_ning/pkg/protoc"
	"fmt"
	"github.com/astaxie/beego/logs"
	"github.com/go-redis/redis/v8"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/securecookie"
	"google.golang.org/protobuf/proto"
	"net"
	"os"
	"time"
)

var db *sql.DB
var err error
var hashKey = []byte("abcdefg")
var s = securecookie.New(hashKey, nil)
var tcpConfig config.TcpConfig
var rdb *redis.Client
var ctx = context.Background()

func init() {
	configFile, err := os.Open("/Users/zhiyuan.ning/go/src/entry_task_ning/config/config.json")
	defer configFile.Close()
	if err != nil {
		fmt.Println(err.Error())
	}
	jsonParser := json.NewDecoder(configFile)
	jsonParser.Decode(&tcpConfig)

	rdb = redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
		PoolSize: 100,
	})
}

func main() {
	// Open up our database connection.
	db, err = sql.Open(tcpConfig.DatabaseDriverName, tcpConfig.DataSourceName)
	if err != nil {
		panic(err)
	}

	db.SetConnMaxLifetime(time.Minute * 3)
	db.SetMaxOpenConns(tcpConfig.DatabaseMaxOpenConns)
	db.SetMaxIdleConns(tcpConfig.DatabaseMaxIdleConns)
	defer db.Close()

	startServer()
}

func startServer() {
	// start the server
	l, err := net.Listen("tcp", tcpConfig.TCPServerAddress)
	if err != nil {
		fmt.Println(err)
	}
	logs.Debug("Open the TCP server and start to accept the connection through :8081 port")
	for {
		if conn, err := l.Accept(); err == nil {
			logs.Debug(conn, "Start a new connection")
			go handleRequest(conn)
		}
	}
}

func handleRequest(conn net.Conn) {
	for {
		//Make a buffer to hold incoming data
		data := make([]byte, 4096)
		//Read the incoming data into the buffer
		length, err := conn.Read(data)
		if err != nil || length == 0 {
			continue
		}
		//Create a message for this request
		message := __.Message{}
		//Deserialize the data
		proto.Unmarshal(data[:length], &message)
		requestType := message.Type

		switch requestType {
		case "LoginReq":
			fmt.Println("This is a login req")
			req := &__.LoginReq{}
			proto.Unmarshal(message.Req, req)
			loginHandler(req, conn)
		case "UpdateReq":
			fmt.Println("This is a update req")
			req := &__.UpdateReq{}
			proto.Unmarshal(message.Req, req)
			updateHandler(req, conn)
		case "UploadReq":
			fmt.Println("This is a upload req")
			req := &__.UploadReq{}
			proto.Unmarshal(message.Req, req)
			uploadHandler(req, conn)
		default:
			fmt.Println("This is not a good request")
		}
	}

}

package main

import (
	"encoding/json"
)

type cacheRes struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Nickname string `json:"nickname"`
	Picture  string `json:"picture"`
}

func getCacheUserInfo(username string) (usernameCache, passwordCache, nicknameCache, pictureCache string) {
	cacheRes := &cacheRes{}
	val, _ := rdb.Get(ctx, username).Result()
	if err != nil {
		return "", "", "", ""
	}
	json.Unmarshal([]byte(val), cacheRes)
	return cacheRes.Username, cacheRes.Password, cacheRes.Nickname, cacheRes.Picture
}

func setCacheUserInfo(username, password, nickname, picture string) {
	cacheRes := &cacheRes{
		Username: username,
		Picture:  picture,
		Nickname: nickname,
		Password: password,
	}
	val, _ := json.Marshal(cacheRes)
	err := rdb.Set(ctx, username, val, 0).Err()
	if err != nil {
		panic(err)
	}
}

package main

import (
	"fmt"
	"github.com/astaxie/beego/logs"
)

func getDbUserInfo(username string) (name, psw, nick, pic string) {
	var usernameDb, passwordDb, nicknameDb, pictureDb string

	err := db.QueryRow(
		"SELECT username, password, nickname, picture FROM ET_USER WHERE username=?",
		username).Scan(
		&usernameDb,
		&passwordDb,
		&nicknameDb,
		&pictureDb)
	if err != nil {
		logs.Error("userinfo failed for user:", username, " with err:")
	}
	return usernameDb, passwordDb, nicknameDb, pictureDb
}

func updateDbNickname(username, nickname string) {
	_, err := db.Exec(
		"UPDATE ET_USER SET nickname=? WHERE username=?",
		nickname,
		username)
	if err != nil {
		logs.Critical(username, "updateDBNickname error update")
		return
	}
}

func uploadDbPicture(username, picture string) {
	_, err := db.Exec(
		"UPDATE ET_USER SET picture=? WHERE username=?",
		picture,
		username)
	if err != nil {
		fmt.Println(err)
		logs.Critical(username, "uploadDbPicture error")
		return
	}
}

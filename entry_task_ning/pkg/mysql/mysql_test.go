package main

import (
	"database/sql"
	"fmt"
	"github.com/astaxie/beego/logs"
	_ "github.com/go-sql-driver/mysql"
	"testing"
	"time"
)

func TestMysql(t *testing.T) {

	db, err := sql.Open("mysql", "root:@/entry_task")
	if err != nil {
		panic(err)
	}

	logs.Debug("Opened the mysql database")

	db.SetConnMaxLifetime(time.Minute * 3)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)

	var username_db, password_db, nickname_db, picture_db string

	err = db.QueryRow("SELECT username, password, nickname, picture FROM ET_USER WHERE username=?", "testUser").Scan(&username_db, &password_db, &nickname_db, &picture_db)
	fmt.Println(picture_db)
	fmt.Println("success")
	db.Close()
}

package main

import (
	"os"
	"strconv"
)

func main() {
	file, err := os.Create("fake_user.txt")
	if err != nil {
		return
	}
	defer file.Close()

	for i := 0; i < 200; i++ {
		t := strconv.Itoa(i)
		str := "U" + t + ",fakeP,FakeN,fakePic.jpg"
		file.WriteString(str)
		file.WriteString("\n")
	}
}

// LOAD DATA INFILE '/Users/zhiyuan.ning/go/src/entry_task_ning/pkg/mysql/fake_user.txt' INTO TABLE user_tab;


CREATE TABLE `user_tab` (
                            `idx_user_username` varchar(45) NOT NULL,
                            `user_password` varchar(300) DEFAULT NULL,
                            `user_nickname` varchar(45) DEFAULT NULL,
                            `user_picture` varchar(300) DEFAULT NULL,
                            PRIMARY KEY (`idx_user_username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

SET SQL_SAFE_UPDATES = 0;

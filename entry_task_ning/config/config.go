package config

type TcpConfig struct {
	DatabaseMaxIdleConns int    `json:"DatabaseMaxIdleConns"`
	DatabaseDriverName   string `json:"DatabaseDriverName"`
	DataSourceName       string `json:"DataSourceName"`
	DatabaseMaxOpenConns int    `json:"DatabaseMaxOpenConns"`
	TCPServerAddress     string `json:"TCPServerAddress"`
}

type HttpConfig struct {
	TCPServerAddress  string `json:"TCPServerAddress"`
	HTTPServerAddress string `json:"HTTPServerAddress"`
	HTMLFilePath      string `json:"HTMLFilePath" `
}
